# BlockchainAuction

Web application for serving an auction using blockhain and smartcontracts.

---

## Tech stack

* **Backend**
  * Django
* **Frontend**
  * React
* **Smart contracts**
  * Ethereum VM
  * Truffle
  * Ganache

---

## Frontend installation

You should have: Node.JS >= 8.10.0, yarn or npm.

---

### Node.JS installation

(I recommend to use node version manager)

``` bash
sudo apt-get update
sudo apt-get install build-essential libssl-dev
curl -sL https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh -o install_nvm.sh
bash install_nvm.sh
```

After that you should restart your console and :

``` bash
nvm install 8.10.0
nvm use 8.10.0
node -v
nvm alias default 8.10.0
```

---

### Project installation

``` bash
yarn install
```

or

``` bash
npm install
```

### Run

``` bash
yarn start
```

---

## Backend installation

``` bash
docker-compose build
docker-compose up
```

Посмотреть тестовые приватные ключи можно здесь

``` bash
docker-compose logs ganache
```
