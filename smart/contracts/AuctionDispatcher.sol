pragma solidity ^0.4.22;

// Import simple auction contract
import {Auction} from "./Auction.sol";

contract AuctionDispatcher {
    
    // Save all auctions addresses
    address[] public auctions;
    
    // Information abour all users. 0 - nobody; 1 - user; 2 - seller; 3 - owner.
    mapping(address => uint) public userGroups;

    // Lot ID counter, start from 0
    uint lotIDcounter;
    address superOwner;

    // When new user is created get its address
    event NewUser(address e_userAddress);
    event OnCreateAuction(address indexed _from, uint256 _value);

    constructor() public {
        userGroups[msg.sender] = 3;
    }

    // Add one more user
    function addUser(address userAddress) public {
        userGroups[userAddress] = 1;
    }

    // Show user group by address
    function getUserGroup(address userAddress) public view returns (uint) {
        return userGroups[userAddress];
    }

    // Change user group, only owner
    function changeUserGroup(address userAddress, uint newGroup) public {
        if(userGroups[msg.sender] != 3) {
            revert("Go fuck yourself");
        }
        userGroups[userAddress] = newGroup;
    }

    // Seller or owner send an application for auction
    function createAuction(
        uint lotID,
        uint minPrice,
        uint minStep,
        uint maxStep,
        uint startTime,
        uint endTime
        ) public returns (uint) {
        Auction newAuction = new Auction(
            msg.sender,
            msg.sender,
            lotID,
            minPrice,
            minStep,
            maxStep,
            startTime,
            endTime
        );

        // If it is not seller or owner
        if(userGroups[msg.sender] < 2)  {
            return 17;
        }

        auctions.push(newAuction);
        emit OnCreateAuction(msg.sender, 0);

        return 0;
    }

    function getAuctions() public view returns (address[])
    {
//        address[] memory data = new address[](auctions.length);
//
//        for (uint i = 0; i < auctions.length; i++) {
//            data[i] = auctions[i];
//        }

        return auctions;
    }

    function getLastAuction() public view returns (address)
    {
        return auctions[auctions.length-1];
    }
}
