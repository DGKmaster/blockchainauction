pragma solidity ^0.4.22;

// 1 - Only for first Bet
// 2 - More money than min price
// 3 - Less money than min price + max step
// 4 - More money
// 5 - Less money
// 6 - onlyNotSeller
// 7 - First pend first bet above min price
// 8 - onlyAfterStart
// 9 - onlyNotAborted
// 10 - Low than best bet
// 11 - only Seller or Owner
// 12 - onlyBeforeStart
// 13 - onlyBeforeEnd
// 14 - No end of auction
// 15 - Not finished
// 16 - Only owner
// 17 - Go fuck yourself

contract Auction {
    // Master addresses
    address public owner;
    address public seller;

    // Start and end time of auction
    uint public startTime;
    uint public endTime;

    // Price config
    uint public minPrice;
    uint public minStep;
    uint public maxStep;
    
    // Lot ID to match it with event
    uint public lotID;
    
    // All information about best bet
    uint public bestBet;
    address public bestBuyer;
    // Log of all bets
    uint[] public allBets;
    address[] public allBuyers;

    // Log for storing info about who get money back
    uint[] public returnBets;
    address[] public returnBuyers;

    // Save all buyers money transfers
    mapping(address => uint256) public buyersMoney;
    
    // States
    bool public aborted;
    bool public firstBet;
    bool public finished;

    // Get new information about bet and change logs
    event UpdateBetsBuyers(uint[] e_allBets, address[] e_allBuyers, uint e_bestBet, address e_bestBuyer, uint e_lotID);
    // Get information about winner
    event Winner(uint e_bestBet, address e_bestBuyer, uint e_lotID);
    // Get information about money transfer back to buyer
    event NewReturn(uint e_bet, address e_buyer,  uint e_lotID);
    // Informate if auction is aborted
    event Aborted();

    // Base constructor for init contract
    constructor(
        address _owner, 
        address _seller,
        uint _lotID,
        uint _minPrice, 
        uint _minStep, 
        uint _maxStep, 
        uint _startTime, 
        uint _endTime) public {
        
        // Check initial conditions
        if(_startTime >= _endTime) {
            revert("Start and End BAD");
        }
        if(_startTime > block.number) {
            revert("Start time BAD");
        }
        
        // Init storage
        owner = _owner;
        seller = _seller;
        lotID = _lotID;
        minPrice = _minPrice;
        minStep = _minStep;
        maxStep = _maxStep;
        startTime = _startTime;
        endTime = _endTime;

        // Init state
        aborted = false;
        firstBet = true;
        finished = false;
    }

    function getLotInfo() view public returns
    (
        address,
        address,
        uint,
        uint,
        uint,
        uint,
        uint,
        uint,
        bool,
        bool,
        bool
        ) {
        return (
            owner, 
            seller, 
            lotID, 
            minPrice, 
            minStep,
            maxStep, 
            startTime, 
            endTime,
            aborted,
            firstBet,
            finished
        );
    }

    // Get information how much money buyer spend on bet
    function getBuyerMoneyAmount(address buyer) view public returns (uint256) {
        return buyersMoney[buyer];
    }

    // Get information what is best bet (in money)
    function getBestBet() view public returns (uint) {
        return bestBet;
    }

    // Get information who has best bet
    function getBestBuyer() view public returns (address) {
        return bestBuyer;
    }

    // Get log about all bets
    function getAllBets() public view returns (uint[], address[]) {
        return (allBets, allBuyers);
    }   

    // Check all conditions before place a bet
    function checkBet(uint value) public returns (uint success) {
        // Calculate bet parameters
        uint newBet = buyersMoney[msg.sender] + value;
        uint difBet = newBet - bestBet;

        // If it is first bet check min price
        if(firstBet) {  
            // Check min and max price
            if(value < minPrice) {
                return 2;
            }
            if(value > minPrice + maxStep) {
                return 3;
            }
            
            // Change state
            firstBet = false;
        }
        // If it is not first bet check dif
        else {
            // Check Bet step
            if(difBet < minStep) {
                return 4;
            }
            if(difBet > maxStep) {
                return 5;
            }
            if(msg.sender == seller) {
                return 6;
            }
            if(block.number > endTime) {
                finishAuction();
                return 1;
            }
            
            if(block.number < startTime) {
                return 8;
            }
            if(aborted) {
                return 9;
            }
            if(newBet <= bestBet) {
                return 10;
            }
        }

        return 0;
    }

    // Check finish condition and call finish function
    function checkFinish() public view returns (bool) {
        if(block.number > endTime) {
            return true;
        }
        else {
            return false;
        }
    }

    // Place bets
    function placeBet() public payable returns (uint success) {
        // Calculate total Bet
        uint newBet = buyersMoney[msg.sender] + msg.value;
        
        uint flag = checkBet(msg.value);
        if (flag != 0) {
            return 1;
        }
        
        // Add sent money to log
        buyersMoney[msg.sender] = newBet;
        
        // Update information about highest bet
        bestBet = newBet;
        bestBuyer = msg.sender;

        // Log bets and buyers
        allBets.push(bestBet);
        allBuyers.push(msg.sender);

        // Inform front-end
        emit UpdateBetsBuyers(allBets, allBuyers, bestBet, bestBuyer, lotID);

        return 0;
    }
    
    // Change min price before auction
    function setMinPrice(uint newMinPrice) public returns (uint success) {
        if((msg.sender != seller) || (msg.sender != owner)) {
            return 11;
        }
        if(block.number > startTime) {
            return 12;
        }
        minPrice = newMinPrice;
        
        return 0;
    }

    // Abort auction by seller
    function abortAuction() public returns (uint success) {
        if((msg.sender != seller) || (msg.sender != owner)) {
            return 11;
        }
        if(block.number > endTime) {
            return 13;
        }
        // Change state
        aborted = true;

        // Inform
        emit Aborted();

        // Allow get money back
        finishAuction();

        return 0;
    }

    // Finish auction if time is over or it is aborted
    function finishAuction() public returns (uint success) {
        if(block.number < endTime) {
            return 14;
        }
        uint sellMoney;
        uint commission;

        // If it is not aborted save money from end buyer
        if(!aborted) {
            sellMoney = buyersMoney[bestBuyer];
            
            // Calculate commission
            commission = sellMoney/100;
            sellMoney = sellMoney - commission;

            // Send money
            seller.transfer(sellMoney);
            owner.transfer(commission);

            emit Winner(bestBet, bestBuyer, lotID);
        }
        
        // Change state
        finished = true;

        return 0;
    }

    // If auction is finished allow get money back
    function sendMoneyBack() public returns (uint success) {
        // Check state
        if(!finished) {
            return 15;
        }

        // Get all money that the buyer has sent
        uint256 amount = buyersMoney[msg.sender];
        
        // Transfer money to caller
        msg.sender.transfer(amount);

        returnBets.push(amount);
        returnBuyers.push(msg.sender);

        // Zero caller money
        buyersMoney[msg.sender] = 0;

        // Inform about money return
        emit NewReturn(amount, msg.sender,  lotID);

        return 0;
    }

    // Get info about returned money
    function getReturnInfo() public view returns (uint[], address[]) {
        return (returnBets, returnBuyers);
    }

    // Finish contract by owner
    function finishContract() public returns (uint success) {
        if(msg.sender != owner) {
            return 16;
        }
        selfdestruct(owner);

        return 0;
    }
}