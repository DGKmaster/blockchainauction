pragma solidity ^0.4.18;

contract AuctionDispatcher {

    event NewDeveloper(uint devId, string name, uint age);
    event EventOrder(uint orderId); // customer
    event EventOffer(uint offerId, uint indexed orderId); // carrier
    event EventRespond(uint indexed offerId); // customer
    event EventInvoice(uint invoiceId, uint indexed orderId); // carrier
    event EventPayment(uint indexed invoiceId); // customer
    event EventFulfill(uint indexed orderId); // carrier
    event EventRefund(uint indexed invoiceId); // carrier
    event EventReview(uint indexed orderId, address from); // customer || carrier
    event EventWithdrawFees(uint fees); // oslikiFoundation

    struct Developer {
        string name;
        uint id;
        uint age;
    }

    struct Order {
        address customer;
        string from; // Geographic coordinate in 'lat,lon' format or Ethereum address '0x...'
        string to; // Geographic coordinate in 'lat,lon' format or Ethereum address '0x...'
        string params; // Package params in 'weight(kg),length(m),width(m),height(m)' format
        uint expires; // Expiration date in SECONDS since Unix Epoch
        string message;

        uint[] offerIds; // Array of carrier offers

        address carrier; // Chosen carrier
        uint invoiceId;

        uint createdAt;
        uint updatedAt;
    }

 
    struct Offer {
        address carrier;
        uint orderId;
        string message;
        string respond; // Client respond
        uint createdAt;
        uint updatedAt;
    }

    struct Invoice {
        address sender; // Carrier
        uint orderId;
        uint prepayment; // Amount for the prepayment
        uint deposit; // Amount for the deposit

        uint expires; // Expiration date in SECONDS since Unix Epoch
        bytes32 depositHash; // Ethereum-SHA-3 (Keccak-256) hash of the deposit key string provided by the customer

        uint createdAt;
        uint updatedAt;
    }

    struct Stat {
        uint[] orders;
        uint rateSum;
        uint rateCount; // averageRate = rateSum / rateCount
        mapping (uint => Review) reviews; // mapping orderId => Stat
    }

    struct Review {
        uint8 rate; // Range between 1-5
        string text;
        uint createdAt;
    }

    address public oslikiFoundation; // Osliki Foundation (OF) address
    uint8 public constant OSLIKI_FEE = 1; // Only for transactions in ETH (1%)
    uint public fees = 0; // To know how much can be withdrawn in favor of the Osliki Foundation

    uint maxAge = 100;
    uint minAge = 5; 
    Order[][] public orders;
    Offer[] public offers;
    Invoice[] public invoices;
    mapping (address => Stat) internal stats; // Statistics for each user who ever used the platform

    Developer[] public developers;

    mapping (uint => address) public devToOwner;
    mapping (address => uint) public ownerDevCount;

    // because default value of the invoiceId in all orders == 0
    /*
    invoices.push(Invoice({
      sender: 0x0,
      orderId: 0,
      prepayment: 0,
      deposit: 0,
      currency: EnumCurrency.ETH,
      expires: 0,
      depositHash: 0x0,
      status: EnumInvoiceStatus.New,
      createdAt: now,
      updatedAt: now
    }));
    }
    */
    
    function _createDeveloper(string _name, uint _id, uint _age) private{
        uint id = developers.push(Developer(_name, _id, _age)) - 1;
        ownerDevCount[msg.sender]++;
        devToOwner[id] = msg.sender;
        emit NewDeveloper(id, _name, _age);
    }

    function _generateRandomId() private pure returns (uint){
        uint rand = 1;
        return rand;
    }

    function createRandomDeveloper(string _name, uint _age) public payable {
        require(_age > minAge);
        require(_age < maxAge);
        require(msg.value == 5000000000000000000);
        uint randId = _generateRandomId();
        _createDeveloper(_name, randId, _age);
    }

    function getAllDevelopers() public view returns (uint) {
        return developers.length;
    }
}

/*
pragma solidity ^0.4.17;

contract SimpleStorage {
    uint myVariable;

    function set(uint x) public {
        myVariable = x;
    }

    function get() view public returns (uint) {
        return myVariable;
    }

    function setget() view public {
    
    }
}


pragma solidity ^0.4.17;

contract SimpleStorage {
  uint myVariable;

  function set(uint x) public {
  while(true) {
    myVariable = x;
  }
}

  function get() constant public returns (uint) {
    return myVariable;
  }
}
*/
