import React from 'react';
import ReactDOM from 'react-dom';
import {createStore, compose, applyMiddleware} from "redux"
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

import './index.css';
import App from './App';
import allReducers from "./reducers";
import { AUTH_USER } from "./reducers/consts";
import registerServiceWorker from './registerServiceWorker';

const store = createStore(allReducers, compose(applyMiddleware(thunk)));

const token = localStorage.getItem('token')

if (token && (token !== 'undefined')) {
    console.log('TOKEN TRUE');
    store.dispatch({type: AUTH_USER})
}

ReactDOM.hydrate(
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>, document.getElementById('root'));

registerServiceWorker();
