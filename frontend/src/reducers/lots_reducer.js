
import {GET_BETS, UPDATE_LOTS} from "./consts";

export default function lotsReducer(state = { lots: [], bets: [[],[]] }, action) {
    switch (action.type) {
        case GET_BETS:
            return {...state, bets: action.bets }
        case UPDATE_LOTS:
            return {...state, lots: action.lots }
        default:
            return state
    }
}