import {combineReducers} from 'redux'
import authReducer from './auth_reducer'
import profileReducer from './profile_reducer'
import lotsReducer from './lots_reducer'

const allReducers = combineReducers({
    auth: authReducer,
    profile: profileReducer,
    auction: lotsReducer
});

export default allReducers