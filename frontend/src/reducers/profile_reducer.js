
import {GET_BALANCE} from "./consts";

export default function profileReducer(state = { balance: 0.00  }, action) {
    switch (action.type) {
        case GET_BALANCE:
            return {...state, balance: action.balance }
        default:
            return state
    }
}