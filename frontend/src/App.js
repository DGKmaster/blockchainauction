import React, { Component } from 'react';

import {Route, Link, Redirect, Switch, withRouter} from 'react-router-dom'
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import styles from './App.sass';
import Landing from './pages/Landing/Landing'
import Login from './pages/Login/Login'
import Logout from './pages/Login/Logout'
import Main from './pages/Main/Main'
import PrivateRoute from './privateRoute'
import Registration from './pages/Registration/Registration'
import './assets/antd.css'
import './assets/reset.css'

class App extends Component {
  render() {
    return (
      <div className={styles.App}>
          <div className={styles.app}>
              <Switch>
                  <Route path="/" exact component={Landing}/>
                  <Route path="/login" exact component={Login}/>
                  <Route path="/signout" exact component={Logout}/>
                  <Route path="/registration" exact component={Registration}/>
                  <PrivateRoute path="/main" auth={this.props.auth} component={Main}/>
              </Switch>
          </div>
      </div>
    );
  }
}

/*
* <Route path="/login" exact component={CheckoutPage}/>
                  <Route path="/registration" exact component={CheckoutPage}/>
                  <Route path="/account" exact component={SuccessPage}/>
                  <Route path="/signout" exact component={Signout}/>
                  <PrivateRoute path="/main" auth={this.props.auth} component={Main}/>*/

const mapStateToProps = (state) => ({
    auth: state.auth.authenticated
});
const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);
export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(App))
