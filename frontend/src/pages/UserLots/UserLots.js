import React, {Component} from 'react'
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Table, Button, Icon, Dropdown, Menu, Spin, Modal, Input, Popconfirm, message} from 'antd'
import styles from './UserLots.sass'
import moment from 'moment'

class UserLots extends Component {
    constructor(props) {
        super(props)
        this.state = {
        };
    }

    columns = [{
        title: 'TITLE',
        dataIndex: 'name',
        width: '15%',
        onFilter: (value, record) => record.name.indexOf(value) === 0,
        sorter: (a, b) => {
            const compA = a.name.toUpperCase();
            const compB = b.name.toUpperCase();
            return (compA > compB) ? -1 : (compA < compB) ? 1 : 0;
        },
    }, {
        title: 'DESCRIPTION',
        dataIndex: 'description',
        render: (text, record) => {
            if (text.length > 50)
                return text.substring(0,50) + '...'
            else
                return text
        }
    }, {
        title: 'EXPIRE DATE',
        dataIndex: 'date',
        width: '20%',
        sorter: (a, b) => new Date(a.date) - new Date(b.date),
    }, {
        title: 'PRICE',
        dataIndex: 'price',
        width: '15%',
        sorter: (a, b) => {
            let newA = Math.round(+a.cost.replace(/\$\s?|(,*)/g, ''))
            let newB = Math.round(+b.cost.replace(/\$\s?|(,*)/g, ''))
            return newA - newB
        },
    }, {
        title: '',
        dataIndex: 'action',
        width: '5%',
        render: (text, record) => {
            const menu = (
                <Menu className={styles.dropdown}>
                    <Menu.Item>
                        <div onClick={() => this.editItem(record)}>Edit</div>
                    </Menu.Item>
                    <Menu.Item>
                        <Popconfirm placement="topRight" overlayClassName={styles.confirm} title="Sure to delete?"
                                    onConfirm={() => this.onDelete(record.key)}>
                            <a href="javascript:;">Delete</a>
                        </Popconfirm>
                    </Menu.Item>
                </Menu>
            );
            return (
                <Dropdown overlay={menu} trigger={['click']} placement={"bottomRight"}>
                    <a style={{
                        pointerEvents: 'none',
                        color: 'lightgray'
                    }}  className="ant-dropdown-link" href="#">
                        <Icon className="table-row-button" type="ellipsis"/>
                    </a>
                </Dropdown>
            )
        }
    }];


    render() {
        const {selectedRowKeys} = this.state;
        let {count, loading} = this.state;
        let items = this.props.items ? this.props.items.data : null;
        let data = [];
        items = [
            {
                id: 1,
                name: 'Ferrari',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet.',
                date: '06.14.2018',
                price: 120
            }
        ]
        if (items) {
            for (let i = 0; i < items.length; i++) {
                data.push({
                    key: items[i].id,
                    name: items[i].name,
                    description: items[i].description,
                    date: moment(items[i].date, 'MM.DD.YYYY').format('MM.DD.YYYY'),
                    price: items[i].price
                });
            }
        }

        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange,
            hideDefaultSelections: true,
            selections: [{
                key: 'all-data',
                text: 'All',
                onSelect: () => {
                    let arr = [];
                    console.log('data ', data);
                    data.forEach((item, i) => {
                        arr.push(item.key)
                    })
                    this.setState({
                        selectedRowKeys: arr,
                        visibleDeleteButton: true
                    });
                    console.log(this.state);
                },
            },
                {
                    key: 'none',
                    text: 'None',
                    onSelect: () => {
                        this.setState({
                            selectedRowKeys: [],
                            visibleDeleteButton: false
                        });
                        console.log(this.state);
                    },
                }],
            onSelection: this.onSelection,
        };

        return (
            <div className={styles.mylots}>
                <header>
                    <h2>My lots</h2>
                </header>
                <main>
                    <Table className={styles.table} rowClassName={styles.rows} rowSelection={rowSelection}
                           columns={this.columns} loading={loading} dataSource={data}
                           onChange={this.handleChange}/>
                </main>
            </div>
        );
    }
}
const mapStateToProps = (state) => ({

});
const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UserLots);