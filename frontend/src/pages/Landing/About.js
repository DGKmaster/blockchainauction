import React, {Component} from 'react'
import styles from './Landing.sass'
import {NavLink} from 'react-router-dom'
import {Row, Button} from 'antd'
import bg from '../../assets/images/ethbg.png'

class About extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <header className={styles.about}>
                <img src={bg} alt=""/>
            </header>
        );
    }
}

export default About;