import React, {Component} from 'react'
import styles from './Landing.sass'
import {NavLink} from 'react-router-dom'
import {Row, Col, Button} from 'antd'
import Header from './Header'
import bg from '../../assets/images/landbg.png'
import example from '../../assets/images/example.png'
import About from "./About";
import Footer from "./Footer";

class Landing extends Component {

    render() {
        console.log(process.env.REACT_APP_ROOT_URL);
        return (
            <div>
                <div className={styles.home}>
                    <Header/>
                    <img src={bg} alt=""/>
                    <h1>Transparent <span style={{
                        color: "#ce9ed4"
                    }}>E</span>-biddings</h1>
                    <NavLink to={`/login`}>
                        <Button>Join now</Button>
                    </NavLink>
                    <img src={example} alt=""/>
                </div>
            </div>
        );
    }
}

export default Landing;