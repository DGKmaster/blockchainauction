import React, {Component} from 'react'
import styles from './Landing.sass'
import {NavLink} from 'react-router-dom'
import {Row, Button} from 'antd'
import logo from '../../assets/images/mainlogo.png'
import ethlogo from '../../assets/images/ethlogo.png'

class Header extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <header className={styles.header}>
                <NavLink activeClassName={styles.activeHeader} exact to={`/`}>
                    <img src={logo}/>
                </NavLink>
                <div className={styles.navhead}>
                    <NavLink to={`/registration`}>
                        Sign up
                    </NavLink>

                    <NavLink to={`/login`}>
                        Sign in
                    </NavLink>

                    <img src={ethlogo} alt=""/>
                </div>
            </header>
        );
    }
}

export default Header;