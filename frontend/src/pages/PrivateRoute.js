import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom'
export default ({ component: Component, auth: auth,  ...rest }) => {
    return <Route {...rest} render={(props) => (
        auth ? <Component {...props} /> : <Redirect exact to={{pathname: '/login', state: {from: props.location}}} />
    )} />
}