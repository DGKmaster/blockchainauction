import React, {Component} from 'react'
import {bindActionCreators} from 'redux';
import {registerUser}  from '../../actions'
//import {authError}  from '../../actions/errors'
import {connect} from 'react-redux'
import {Form, Icon, Input, Button, Checkbox, message, notification} from 'antd';
import {NavLink, Redirect} from 'react-router-dom'
import styles from './Registration.sass'
//import {NO_LICENCE} from "../../reducers/consts";
const FormItem = Form.Item;
const { TextArea } = Input;

class RegForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            err_message: false,
            redirect: false
        };
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, {username, email, password, private_key}) => {
            console.log(email, password);
            if (!err) {
                console.log('Received values of form');
            }
            if (email && password)
                this.props.registerUser({username, email, password, private_key}).then((data) => {
                    if (data) {
                        if (data.error)
                            this.setState({
                                err_message: true,
                                message: data.message ? data.message : "This data is already exist!"
                            })
                        else this.setState({
                            redirect: true
                        })

                    } else {
                        this.setState({
                            redirect: true
                        })
                    }

                })
        });

    }
    hideError = () => {
        this.setState({
            err_message: false
        })
    }

    validateToNextPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && this.state.confirmDirty) {
            form.validateFields(['confirm'], { force: true });
        }
        callback();
    }
    handleConfirmBlur = (e) => {
        const value = e.target.value;
        this.setState({ confirmDirty: this.state.confirmDirty || !!value });
    }
    compareToFirstPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && value !== form.getFieldValue('password')) {
            callback('Two passwords that you enter is inconsistent!');
        } else {
            callback();
        }
    }

    render() {
        const errorMessage = this.state.err_message ?
            <span className={styles.error}>{this.state.err_message}</span> : ''
        const {getFieldDecorator} = this.props.form;
        return ((this.state.redirect) ?
                <Redirect exact to={{
                    pathname: '/login', state: {
                        from: this.props.location
                    }
                }}/>
                :
                <div className={styles.form}>
                <div className={styles.loginForm}>
                    <h2 className={styles.header}>Registration</h2>
                    <Form onSubmit={this.handleSubmit}>
                        <FormItem>
                            {getFieldDecorator('username', {
                                rules: [{required: true, message: 'Please input your name!'}],
                            })(
                                <Input type='text' placeholder="Name"/>
                            )}
                        </FormItem>
                        <FormItem>
                            {getFieldDecorator('email', {
                                rules: [{required: true, message: 'Please input your email!'}],
                                initialValue: this.props.email
                            })(
                                <Input type="email" placeholder="Email"/>
                            )}
                        </FormItem>
                        <FormItem>
                            {getFieldDecorator('password', {
                                rules: [{
                                    required: true, message: 'Please input your password!',
                                }, {
                                    validator: this.validateToNextPassword,
                                }],
                            })(
                                <Input type="password" placeholder="password"/>
                            )}
                        </FormItem>
                        <FormItem>
                            {getFieldDecorator('confirm', {
                                rules: [{required: true, message: 'Please confirm your password!'}, {
                                    validator: this.compareToFirstPassword,
                                }],
                            })(
                                <Input type="password" placeholder={"Repeat password"} onBlur={this.handleConfirmBlur}/>
                            )}
                        </FormItem>
                        <FormItem>
                            {getFieldDecorator('private_key', {
                                rules: [{required: true, message: 'Please input your wallet key!'}],
                                initialValue: this.props.email
                            })(
                                <Input placeholder="Private key" rows={3}/>
                            )}
                        </FormItem>
                        {this.state.err_message ? this.state.message : ''}
                        <Button type="primary" htmlType="submit" className={"login-form-button " + styles.submit}>
                            Sign up
                        </Button>
                    </Form>
                </div>
            </div>
        );
    }
}

const WrappedRegForm = Form.create()(RegForm);
const mapStateToProps = (state) => ({
    /*email: state.auth.email,
    error: state.auth.error */
});
const mapDispatchToProps = (dispatch) => bindActionCreators({
    registerUser,
    //authError
}, dispatch);
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(WrappedRegForm);