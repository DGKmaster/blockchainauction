import React, {Component} from 'react'
import RegForm from './RegForm'
import {connect} from 'react-redux'
import {Redirect, NavLink} from 'react-router-dom'
import styles from './Registration.sass'
import logo from '../../assets/images/mainlogo.png'

class Registration extends Component {

    componentWillUnmount() {
        /*if (this.props.errorMessage) {
            this.props.authError(null)
        }*/
    }
    displayRedirectMessages() {
        const location = this.props.location
        console.log("location.state", location.state)
        /*if (location.state)
            return <div className="alert alert-danger">{location.state.message}</div>*/
    }

    render() {
        return (
                <div className={styles.formWrapper}>
                    <span className={styles.logotype}>
                    <NavLink to={`/`}>
                        <img src={logo}/>
                    </NavLink>
                </span>
                    <RegForm/>
                    <div className={styles.label}>Already have an account? <span><NavLink
                        to={`/login`}>Login</NavLink></span>
                    </div>
                    <footer className={styles.footer}>
                        <div>FastTrade © 2018 All Rights Reserved.</div>
                    </footer>
                </div>)
    }
}
function mapStateToProps(state) {
    return {
        /*authenticated: state.auth.authenticated,
        errorMessage: state.auth.error,
        redirect: state.auth.redirect*/
    }
}
export default connect(mapStateToProps, {})(Registration)