import React, {Component} from 'react'
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import moment from 'moment'

import {Form, Icon, Input, Button, Checkbox, DatePicker, TimePicker, InputNumber, Card, message, Upload} from 'antd';
import styles from './Lot.sass'
import {createBet} from "../../../actions/lots";

const {TextArea} = Input;
const {Meta} = Card;
const FormItem = Form.Item;

function getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
}

function beforeUpload(file) {
    const isJPG = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJPG) {
        message.error('You can only upload JPG file!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
        message.error('Image must smaller than 2MB!');
    }
    return isJPG && isLt2M;
}

class newBet extends Component {
    constructor(props) {
        super(props)
        let max = 999999999

        this.state = {

            loading: false,
            imageUrl: this.props.item ? this.props.item.imageUrl : false,
            max,
            error: null
        }
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, {bet, private_key}) => {
            if (!err) {
                console.log('Received values of form');
                let imageUrl = this.state.imageUrl ? this.state.imageUrl : '';
                let date = moment(Date.now());
                date = moment(date).format('MM.DD.YYYY')
                this.props.createBet({eth: bet, id: this.props.id, private_key}).then((error) => {
                    if (error) this.setState({error})
                    else this.props.onCancel();
                })

            }
        });
    }
    handlePreview = (file) => {
        this.setState({
            previewImage: file.url || file.thumbUrl,
            previewVisible: true,
        });
    }
    handleCancel = () => this.setState({previewVisible: false})

    handleChange = (info) => {
        if (info.file.status === 'uploading') {
            this.setState({loading: true});
            return;
        }
        if (info.file.status === 'done') {
            let url = info.file.response.data.url;
            // Get this url from response in real world.
            getBase64(info.file.originFileObj, imageUrl => {
                imageUrl = info.file.response.data.url;
                this.setState({
                    imageUrl,
                    loading: false,
                })
            })
            ;
        }
    }

    render() {
        const {getFieldDecorator} = this.props.form;
        const {previewVisible, previewImage, fileList} = this.state;
        const {onClose, item} = this.props;
        const resetConf = {
            id: '',
            name: '',
            brand: '',
            retail_price: '0.00',
            date: new Date(Date.now()),
            qty: 0,
            size: 'S',
            color: '',
            title: 'Add new bet',
            editable: false,
            imageUrl: false,
            operation_id: 1,
            max: 99999999
        };
        const uploadButton = (
            <div>
                <Icon className={styles.uploadPhotoIcon} type={this.state.loading ? 'loading' : 'camera'}/>
                <div className="ant-upload-text"></div>
            </div>
        );
        let baseData = item ? item : resetConf;
        let dateFormat = 'MM.DD.YYYY';
        const config = {
            rules: [{type: 'object', required: true, message: 'Please select time!'}],
            initialValue: moment(baseData.date, dateFormat)
        };
        return (
            <Form onSubmit={this.handleSubmit} className={"login-form "}>
                <header className={styles.modalHeader}>{baseData.title}</header>
                <div className={styles.form}>
                        <FormItem label="YOUR BET">
                            {getFieldDecorator('bet', {
                                rules: [{required: true, message: 'Please input the retail price!'}],
                                initialValue: baseData.retail_price
                            })(<InputNumber
                                min={0}
                                className={styles.inputNumberPrice}
                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                parser={value => value.replace(/\$\s?|(,*)/g, '')}
                            />)}
                            <div className={styles.dollarNumber}>{'ETH'}</div>
                        </FormItem>
                        <FormItem label="PRIVATE KEY">
                            {getFieldDecorator('private_key', {
                                rules: [{required: true, message: 'Please input the brand of collection!'}],
                                initialValue: localStorage.getItem('private_key')
                            })(
                                <Input/>
                            )}
                        </FormItem>
                    <span style={{color: 'red'}}>{this.state.error ? this.state.error : ''}</span>
                </div>
                <FormItem className={styles.submit}>
                    <Button type="primary" htmlType="submit" className={"login-form-button " + styles.submitButton}>
                        {baseData.title.toUpperCase()}
                    </Button>
                </FormItem>
            </Form>
        );
    }
}

const WrappedNewBet = Form.create()(newBet);
const mapStateToProps = (state) => ({
});
const mapDispatchToProps = (dispatch) => bindActionCreators({
    createBet
}, dispatch);
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(WrappedNewBet);