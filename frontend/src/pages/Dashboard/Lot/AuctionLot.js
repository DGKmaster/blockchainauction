import React, {Component} from 'react'
import styles from './Lot.sass'
import {connect} from "react-redux";

import {NavLink} from 'react-router-dom'
import {bindActionCreators} from 'redux';
import { getBets } from '../../../actions/lots'
import {Row, Col, Modal, Button, Icon} from 'antd'
import logo from '../../../assets/images/no-image.png'
import NewBet from './NewBet'

class Lot extends Component {
    constructor(props) {
        super(props)
        this.state = {
            visibleLot: false
        }
    }

    componentDidMount() {
        this.props.getBets(this.props.match.params.id)
    }


    openNewLot = (e) => {
        this.setState({
            visibleLot: true
        })
    }
    handleCancel = (e) => {
        console.log(e);
        this.setState({
            visibleLot: false,
        });
    }
    handleOk = (e) => {
        this.setState({
            visibleLot: false,
        });
    }


    onOpenChange = (openKeys) => {
        const latestOpenKey = openKeys.find(key => this.state.openKeys.indexOf(key) === -1);
        if (this.rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
            this.setState({ openKeys });
        } else {
            this.setState({
                openKeys: latestOpenKey ? [latestOpenKey] : [],
            });
        }
    }

    render() {
        let lots = this.props.lots

        let lot = lots.filter(item => {
            console.log('/main/dashboard/' + item.id);
            if (+item.id === +this.props.match.params.id)
                return item
        })
        let curr_bet = this.props.lots[0].block_info.min_price ? +this.props.lots[0].block_info.min_price : 0;
        if (this.props.bets) this.props.bets[0].forEach((item) => curr_bet += (+item  / 1000000000000000000))
        console.log(this.props);
        console.log(lot);
        return (
                <div className={styles.newsWrapperItem}>
                    <img src={this.props.imageUrl ? this.props.imageUrl : logo}/>
                    <div>
                        <h4>{lot[0].title}</h4>
                        <span>{lot[0].description}</span>
                        <br />
                        <span>{' ' + curr_bet.toFixed(2) + ' ETH'}</span>
                        <div className={styles.date}>
                            <span>{'Start time: '}<b>{this.props.lots[0].block_info.start_time + ' blocks'} </b></span>
                            <br />
                        </div>
                        <div className={styles.date}>
                            <span>{'End time: '}<b>{this.props.lots[0].block_info.end_time + ' blocks'}</b></span>
                            <br/>
                            <span>{' (1 block = 60 min)'}</span>
                        </div>
                        <Button onClick={ () => this.openNewLot()}><Icon type="to-top" />Place a bet</Button>
                        <div className={styles.aucList}>
                            <h4>Buyers</h4>
                            <ul>
                                <li key={-1}><span>{(this.props.lots[0].block_info.min_price ? +this.props.lots[0].block_info.min_price : 0) + ' ETH'}</span><span>min price:</span></li>
                                {
                                   this.props.bets[0].map((item, i) => {
                                       return <li><span>{(this.props.bets[0][i] / 1000000000000000000).toFixed(2) + ' ETH '}</span><span>{this.props.bets[1][i]}</span></li>
                                   })
                                }
                            </ul>
                        </div>
                    </div>
                    <Modal
                        visible={this.state.visibleLot}
                        onOk={this.handleOk}
                        destroyOnClose={true}
                        onCancel={this.handleCancel}
                        footer={[]}
                        width={"400px"}
                    >
                        <NewBet id={this.props.match.params.id} onCancel={this.handleCancel}/>
                    </Modal>
                </div>
        );
    }
}
const mapStateToProps = (state) => (
    {
        lots: state.auction.lots,
        bets: state.auction.bets
    }
);
const mapDispatchToProps = (dispatch) => bindActionCreators(
    {
        getBets
    },
    dispatch
);
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Lot);