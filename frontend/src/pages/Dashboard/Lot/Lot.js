import React, {Component} from 'react'
import styles from './Lot.sass'
import {connect} from "react-redux";

import {NavLink, withRouter} from 'react-router-dom'
import {bindActionCreators} from 'redux';
import {Row, Col, Button, Icon} from 'antd'
import logo1 from '../../../assets/images/no-image.png'
import logo2 from '../../../assets/images/villa.jpg'
import logo3 from '../../../assets/images/builder.jpg'
import moment from 'moment'

class Lot extends Component {
    constructor(props) {
        super(props)
    }

    logoList = {
        1:logo1,
        2:logo1,
        3:logo1
    }

    randomInteger = (min, max) => {
        let rand = min - 0.5 + Math.random() * (max - min + 1)
        rand = Math.round(rand);
        return rand;
    }


    render() {
        let date = moment(this.props.created_at);
        date = moment(date).format('MM.DD.YYYY')
        return (
            <NavLink to={`/main/dashboard/${this.props.lot.id}`}>
                <div className={styles.newsItem}>
                    <img src={this.props.lot.image ? this.props.lot.image : this.logoList[this.randomInteger(1,3)]}/>
                    <div>
                        <h4>{this.props.lot.title}</h4>
                        <span>{this.props.lot.description}</span>
                        <footer className={styles.lotFooter}>
                            <div>
                                <Icon type="calendar"/>
                                <span>{date}</span>
                            </div>
                            <div>
                                <span><Icon type="caret-up" />{' ETH'}</span>
                            </div>
                        </footer>
                    </div>
                </div>
            </NavLink>
        );
    }
}
const mapStateToProps = (state) => (
    {}
);
const mapDispatchToProps = (dispatch) => bindActionCreators(
    {},
    dispatch
);
export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(Lot));