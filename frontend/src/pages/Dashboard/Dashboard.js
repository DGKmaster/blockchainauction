import React, {Component} from 'react'

import {connect} from "react-redux";
import {bindActionCreators} from 'redux';
import {Row, Col, Button} from 'antd'
import styles from './Dashboard.sass'
import Lot from './Lot/Lot'

class Dashboard extends Component {
    constructor(props) {
        super(props)
        this.state = {
            newLoadedItems: [],
            index: 3,
            indexFrom: 0,
            loadedItems: false,
            listLots: []
        }
    }

    loadItems = () => {
        let listLots = [];
        let strLots = [];
        this.props.lots.forEach((item1, i) => {
            if (((i + 1) % 3) !== 0)
                strLots.push(<Lot key={i} lot={item1}/>)
            else {
                strLots.push(<Lot key={i} lot={item1}/>)
                listLots.push(
                    <div key={i} className={styles.blogGrid}>
                        {strLots.map((item2) => item2)}
                    </div>)
                strLots = [];
            }
            if ((i+1) === this.props.lots.length)
                listLots.push(
                    <div key={i} className={styles.blogGrid}>
                        {strLots.map((item2) => item2)}
                    </div>)

        })
        console.log(listLots);
        this.setState({
            listLots: listLots,
            loadedItems: true
        })
    }

    render() {
        const {newLoadedItems} = this.state

        return (
            <div className={styles.home}>
                <h1>Top lots</h1>
                <Row gutter={32}>
                    {
                        this.state.loadedItems ?
                            this.state.listLots.map((item) => item)
                            : <div className={styles.blogGrid}>
                                {this.props.lots.map((item, i) => {
                                    if (i < 3) return <Lot key={i} lot={item}/>
                                })}
                            </div>
                    }
                    {(!this.state.loadedItems && (this.props.lots.length > 0)) ? <Button onClick={this.loadItems}>Load more</Button> : '' }
                </Row>
                <div>FastTrade © 2018 All Rights Reserved.</div>
            </div>
        );
    }
}

const mapStateToProps = (state) => (
    {
        lots: state.auction.lots
    }
);
const mapDispatchToProps = (dispatch) => bindActionCreators(
    {},
    dispatch
);
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Dashboard);