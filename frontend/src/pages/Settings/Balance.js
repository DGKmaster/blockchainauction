import React, {Component} from 'react'
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import styles from './Settings.sass'
import ethlogo from '../../assets/images/ethlogo.png'
const popsicle = require('popsicle');
const defaultCurrencies = 'BTC,USD,EUR,AUD,CHF,CAD,GBP';

const getEthPriceNow = toSymbol => {
    let now = new Date().getTime();
    let ts =  new Date(now);

    // params
    if (typeof toSymbol === 'string') {
        toSymbol = toSymbol.toUpperCase();
    } else {
        toSymbol = defaultCurrencies;
    }

    return popsicle.request({
        method: 'POST',
        url: 'https://min-api.cryptocompare.com/data/price',

        query: {
            fsym: 'ETH',
            tsyms: toSymbol,
            sign: true
        }
    })
        .use(popsicle.plugins.parse(['json']))
        .then(resp => resp.body)
        .then(data => {
            //format  ts : data
            let rtn = {}
            rtn[ts] = { 'ETH': data};
            return rtn});

};


class Balance extends Component {
    constructor(props) {
        super(props)
        this.state = {
            pricer: 0
        };
    }

    componentDidMount() {
        getEthPriceNow()
            .then( data => {
                for (let key in data) {
                    console.log(data[key])
                    this.setState({ pricer: data[key].ETH.USD });
                }
            });
    }

    render() {
        let balance =  this.props.balance ? (this.props.balance / 1000000000000000000).toFixed(2) : 0
        return (
            <div>
                <div className={styles.balance}>
                    <img src={ethlogo} alt=""/>
                    <div>
                        <div><span>{localStorage.getItem('private_key')}</span><span>Wallet ID</span></div>
                        <div>{balance ? balance : '0.00'}<span>ETH</span></div>
                        <div>{(this.state.pricer * balance).toFixed(2)}<span>$</span></div>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) => ({
    balance: state.profile.balance
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
}, dispatch);
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Balance);