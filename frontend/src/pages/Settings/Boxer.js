import React, {Component} from 'react'
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Table, Button, Icon, Dropdown, Menu, Spin, Modal, Input, Popconfirm, message, Divider} from 'antd'
import styles from './Settings.sass'

import Profile from './Profile'
import Balance from './Balance'

class Box extends Component {
    constructor(props) {
        super(props)
        this.state = {
        };
    }

    render() {
        let typeContainer;
        if (this.props.title === 'PROFILE')
            typeContainer = <Profile  title={this.props.title}/>
        else typeContainer = <Balance  title={this.props.title}/>
        return (
                <div className={styles.box}>
                    <h3>{this.props.title}</h3>
                    {typeContainer}
                </div>
        );
    }
}
const mapStateToProps = (state) => ({

});
const mapDispatchToProps = (dispatch) => bindActionCreators({
}, dispatch);
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Box);