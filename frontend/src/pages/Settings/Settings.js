import React, {Component} from 'react'
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Modal} from 'antd'
import styles from './Settings.sass'
import Box from './Boxer'
import 'isomorphic-fetch';
class Settings extends Component {
    constructor(props) {
        super(props)
        this.state = {
        };
    }


    render() {
        return (
            <div className={styles.settings}>
                <header>
                    <h2>Account settings</h2>
                </header>
                <main>
                    <Box title={'PROFILE'}/>
                    <Box title={'BALANCE'} />
                </main>
            </div>
        );
    }
}
const mapStateToProps = (state) => ({
});
const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Settings);