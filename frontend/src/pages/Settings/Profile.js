import React, {Component} from 'react'
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Form, Icon, Input, Button, Checkbox} from 'antd';
import styles from './Settings.sass'

const FormItem = Form.Item;

class Profile extends Component {
    constructor(props) {
        super(props)
        this.state = {};
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
    }

    parseJwt = (token) => {
        let base64Url = token.split('.')[1]
        let base64 = base64Url.replace('-', '+').replace('_', '/')
        return JSON.parse(window.atob(base64))
    }

    render() {
        const {getFieldDecorator} = this.props.form;
        return (
            <Form onSubmit={this.handleSubmit}>
                <div className={styles.profile}>
                    <div>
                        <FormItem label={'Your name'} className={styles.disable}>
                            {getFieldDecorator('username', {
                                rules: [{required: true, message: 'Please input your username!'}],
                                initialValue: localStorage.getItem('username') ? localStorage.getItem('username') : ''
                            })(
                                <Input/>
                            )}
                        </FormItem>
                    </div>
                    <div>
                        <FormItem label={'Email'} className={styles.disable}>
                            {getFieldDecorator('email', {
                                initialValue: localStorage.getItem('token') ? this.parseJwt(localStorage.getItem('token')).email : ''
                            })(
                                <Input/>
                            )}
                        </FormItem>

                    </div>
                    <div>
                        <FormItem label={'Your company'} className={styles.disable}>
                            {getFieldDecorator('company', {
                                initialValue: this.props.company ? this.props.company : ''
                            })(
                                <Input/>
                            )}
                        </FormItem>
                    </div>
                    <div>
                        <Button type="primary" htmlType="submit" className={styles.disable + " login-form-button " + styles.submit}>
                            Save
                        </Button>
                    </div>
                </div>
            </Form>
        );
    }
}

const WrappedProfile = Form.create()(Profile);

const mapStateToProps = (state) => ({});
const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(WrappedProfile);