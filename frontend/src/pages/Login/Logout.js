import React, { Component } from 'react'
import { connect } from 'react-redux'
import { signoutUser } from '../../actions/index'
import { Redirect } from 'react-router-dom'

class Signout extends Component {
    constructor(props) {
        super(props)
        this.props.signoutUser()
    }
    render() {
        return <Redirect exact to='/login' />
    }
}
export default connect(null, { signoutUser })(Signout)