import React, {Component} from 'react'
import LoginForm from './LoginForm'
import {connect} from 'react-redux'
import {Redirect, NavLink} from 'react-router-dom'
import styles from './LoginForm.sass'
import logo from '../../assets/images/mainlogo.png'
class Login extends Component {
    componentWillUnmount() {
        /*if (this.props.errorMessage) {
            this.props.authError(null)
        }*/
    }
    displayRedirectMessages() {
        const location = this.props.location
        console.log("location.state", location.state)
        /*if (location.state)
            return <div className="alert alert-danger">{location.state.message}</div>*/
    }
    getRedirectPath() {
        const locationState = this.props.location.state
        console.log("location ", this.props.location)
        if (locationState && locationState.from) {
            return locationState.from.pathname // redirects to referring url
        } else {
            return '/main'
        }
    }
    render() {
        return (
                <div className={styles.formWrapper}>
                    <span className={styles.logotype}>
                    <NavLink to={`/`}>
                        <img src={logo}/>
                    </NavLink>
                </span>
                    <LoginForm/>
                    <div className={styles.label}>Dont have an account? <span><NavLink
                        to={`/registration`}>Create</NavLink></span>
                    </div>
                    <footer className={styles.footer}>
                        <div>FastTrade © 2018 All Rights Reserved.</div>
                    </footer>
                </div>)
    }
}
function mapStateToProps(state) {
    return {
        /*
        errorMessage: state.auth.error,
        redirect: state.auth.redirect*/
    }
}
export default connect(mapStateToProps, {})(Login)