import React, {Component} from 'react'
import {bindActionCreators} from 'redux';
import {loginUser}  from '../../actions'
import {getBalance}  from '../../actions/profile'
//import {authError}  from '../../actions/errors'
import {connect} from 'react-redux'
import {Form, Icon, Input, Button, Checkbox, message, notification} from 'antd';
import {NavLink, Redirect} from 'react-router-dom'
import styles from './LoginForm.sass'
//import {NO_LICENCE} from "../../reducers/consts";
const FormItem = Form.Item;

class LoginForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            err_message: false,
        };
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, {username, password}) => {
            console.log(username, password);
            if (!err) {
                console.log('Received values of form');
            }
            if (username && password)
                this.props.loginUser({username, password}).then((data) => {
                    if (data)
                        if (data.error)
                            this.setState({
                                err_message: true
                            })
                })
        });

    }
    hideError = () => {
        this.setState({
            err_message: false
        })
    }

    render() {
        const errorMessage = this.props.error ? <span style={{
            color: 'red'
        }}>{this.props.error}</span> : ''
        const {getFieldDecorator} = this.props.form;
        return (this.props.authenticated) ?
            <Redirect exact to={{
                pathname: '/main', state: {
                    from: this.props.location
                }
            }}/>
            : (
            <div className={styles.form}>
                <div className={styles.loginForm}>
                    <h2 className={styles.header}>Login</h2>
                    <Form onSubmit={this.handleSubmit} className="login-form">
                        <FormItem>
                            {getFieldDecorator('username', {
                                rules: [{required: true, message: 'Please input your username!'}],
                                initialValue: this.props.email
                            })(
                                <Input type='text' placeholder="Username" onChange={() => this.hideError()}/>
                            )}
                        </FormItem>
                        <FormItem className={styles.pwd}>
                            {getFieldDecorator('password', {
                                rules: [{required: true, message: 'Please input your password!'}],
                            })(
                                <Input type="password" placeholder="Password" onChange={() => this.hideError()}/>
                            )}
                        </FormItem>
                        {this.state.err_message ? "Bad login info!" : ""}
                        <Button type="primary" htmlType="submit" className={"login-form-button " + styles.submit}>
                            Log in
                        </Button>
                    </Form>
                </div>
            </div>
        );
    }
}

const WrappedLoginForm = Form.create()(LoginForm);
const mapStateToProps = (state) => ({
    authenticated: state.auth.authenticated,
    error: state.auth.error
    /*email: state.auth.email,
    error: state.auth.error */
});
const mapDispatchToProps = (dispatch) => bindActionCreators({
    loginUser,
    getBalance
    //authError
}, dispatch);
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(WrappedLoginForm);