import React, {Component} from 'react'
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import moment from 'moment'
import {ROOT_URL} from '../../../reducers/consts'
import {Form, Icon, Input, Button, Checkbox, DatePicker, TimePicker, InputNumber, Card, message, Upload} from 'antd';
import styles from './NewLot.sass'
import {createLot} from "../../../actions/lots";

const {TextArea} = Input;
const {Meta} = Card;
const FormItem = Form.Item;

function getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
}

function beforeUpload(file) {
    const isJPG = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJPG) {
        message.error('You can only upload JPG file!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
        message.error('Image must smaller than 2MB!');
    }
    return isJPG && isLt2M;
}

class newLot extends Component {
    constructor(props) {
        super(props)
        let max = 999999999

        this.state = {

            loading: false,
            imageUrl: this.props.item ? this.props.item.imageUrl : false,
            max
        }
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, {title, description, date, min_step, max_step, min_price}) => {
            if (!err) {
                console.log('Received values of form');
                let imageUrl = this.state.imageUrl ? this.state.imageUrl : '';
                let d1 = moment(Date.now());
                let d2 = moment(date)
                let interval = new Date(d2 - d1);
                console.log(interval.getSeconds());
                let blocks = (interval.getSeconds() / 60).toFixed(0);
                //date = moment(d1).format('MM.DD.YYYY')
                this.props.createLot({title, description, min_step, max_step, min_price, blocks}).then(() => {
                    this.props.onCancel();
                })

            }
        });
    }
    handlePreview = (file) => {
        this.setState({
            previewImage: file.url || file.thumbUrl,
            previewVisible: true,
        });
    }
    handleCancel = () => this.setState({previewVisible: false})

    handleChange = (info) => {
        if (info.file.status === 'uploading') {
            this.setState({loading: true});
            return;
        }
        if (info.file.status === 'done') {
            let url = info.file.response.data.url;
            // Get this url from response in real world.
            getBase64(info.file.originFileObj, imageUrl => {
                imageUrl = info.file.response.data.url;
                this.setState({
                    imageUrl,
                    loading: false,
                })
            })
            ;
        }
    }

    render() {
        const {getFieldDecorator} = this.props.form;
        const {previewVisible, previewImage, fileList} = this.state;
        const {onClose, item} = this.props;
        const resetConf = {
            id: '',
            name: '',
            brand: '',
            retail_price: '0.00',
            date: new Date(Date.now()),
            qty: 0,
            size: 'S',
            color: '',
            title: 'Add new lot',
            editable: false,
            imageUrl: false,
            operation_id: 1,
            max: 99999999
        };
        const uploadButton = (
            <div>
                <Icon className={styles.uploadPhotoIcon} type={this.state.loading ? 'loading' : 'camera'}/>
                <div className="ant-upload-text"></div>
            </div>
        );
        let baseData = item ? item : resetConf;
        let dateFormat = 'MM.DD.YYYY';
        const config = {
            rules: [{type: 'object', required: true, message: 'Please select time!'}],
            initialValue: moment(baseData.date, dateFormat)
        };
        return (
            <Form onSubmit={this.handleSubmit} className={"login-form "}>
                <header className={styles.modalHeader}>{baseData.title}</header>
                <div className={styles.form}>
                    <div className={styles.leftbar}>
                        <Upload
                            name="image"
                            listType="picture-card"
                            className="avatar-uploader"
                            headers={{
                                'Accept': 'application/json',
                            }}
                            action={"https://api.imgur.com/3/image"}
                            showUploadList={false}
                            beforeUpload={beforeUpload}
                            onChange={this.handleChange}
                        >
                            {baseData.imageUrl ?
                                <img className={styles.image} src={baseData.imageUrl} alt=""/> : uploadButton}
                        </Upload>
                    </div>
                    <div className={styles.rightbar}>
                        <FormItem label="TITLE" className={styles.name}>
                            {getFieldDecorator('title', {
                                rules: [{required: true, message: 'Please input the title!'}],
                                initialValue: baseData.name
                            })(
                                <Input/>
                            )}
                        </FormItem>
                        <FormItem label="DESCRIPTION">
                            {getFieldDecorator('description', {
                                rules: [{required: true, message: 'Please input description!'}],
                                initialValue: baseData.brand
                            })(
                                <Input/>
                            )}
                        </FormItem>
                        <FormItem label="MIN PRICE">
                            {getFieldDecorator('min_price', {
                                rules: [{required: true, message: 'Please input the min price!'}],
                                initialValue: baseData.retail_price
                            })(<InputNumber
                                min={0}
                                className={styles.inputNumberPrice}
                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                parser={value => value.replace(/\$\s?|(,*)/g, '')}
                            />)}
                            <div className={styles.dollarNumber}>{'ETH'}</div>
                        </FormItem>
                        <div className={styles.rightside}>
                            <FormItem label="MIN STEP">
                                {getFieldDecorator('min_step', {
                                    rules: [{required: true, message: 'Please input the min step!'}],
                                    initialValue: baseData.retail_price
                                })(<InputNumber
                                    min={0}
                                    className={styles.inputNumberMinStep}
                                    formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                    parser={value => value.replace(/\$\s?|(,*)/g, '')}
                                />)}
                                <div className={styles.dollarNumber}>{'ETH'}</div>
                            </FormItem>
                            <FormItem label="MAX STEP" >
                                {getFieldDecorator('max_step', {
                                    rules: [{required: true, message: 'Please input the max step!'}],
                                    initialValue: baseData.retail_price
                                })(<InputNumber
                                    min={0}
                                    className={styles.inputMaxStep}
                                    formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                    parser={value => value.replace(/\$\s?|(,*)/g, '')}
                                />)}
                                <div className={styles.dollarNumber}>{'ETH'}</div>
                            </FormItem>
                        </div>
                        <FormItem

                            label="EXPIRE DATE"
                        >
                            {getFieldDecorator('date', config)(
                                <DatePicker format={dateFormat} placeholder=""/>
                            )}
                        </FormItem>

                    </div>
                </div>
                <FormItem className={styles.submit}>
                    <Button type="primary" htmlType="submit" className={"login-form-button " + styles.submitButton}>
                        {baseData.title.toUpperCase()}
                    </Button>
                </FormItem>
            </Form>
        );
    }
}

const WrappedNewLot = Form.create()(newLot);
const mapStateToProps = (state) => ({});
const mapDispatchToProps = (dispatch) => bindActionCreators({
    createLot
}, dispatch);
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(WrappedNewLot);