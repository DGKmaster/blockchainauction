import React, {Component} from 'react'
import {NavLink, withRouter} from 'react-router-dom'
import {bindActionCreators} from 'redux';
import styles from './Navbar.sass'
import {Icon, Modal, Menu} from 'antd'
import {connect} from 'react-redux';
import NewLot from './NewLot'
import logo from '../../../assets/images/whitelogo.png'
const SubMenu = Menu.SubMenu;

class Navbar extends Component {
    constructor(props) {
        super(props)
        this.state = {
            visibleLot: false,
            openKeys: []
        };
    }

    rootSubmenuKeys = ['sub1', 'sub2'];

    openNewLot = (e) => {
        this.setState({
            visibleLot: true
        })
    }
    handleCancel = (e) => {
        console.log(e);
        this.setState({
            visibleLot: false,
        });
    }
    handleOk = (e) => {
        this.setState({
            visibleLot: false,
        });
    }


    onOpenChange = (openKeys) => {
        const latestOpenKey = openKeys.find(key => this.state.openKeys.indexOf(key) === -1);
        if (this.rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
            this.setState({ openKeys });
        } else {
            this.setState({
                openKeys: latestOpenKey ? [latestOpenKey] : [],
            });
        }
    }

    render() {
        return (
            <div className={styles.navbar}>
                <NavLink exact to={`/`}><img className={styles.logotype}
                                             src={logo}/></NavLink>
                <NavLink exact to={`/main/dashboard`}><Icon type="home" />Home</NavLink>
                <div onClick={ () => this.openNewLot()}><Icon type="plus-circle-o" />Make new lot</div>
                <NavLink style={{
                    pointerEvents: 'none',
                    color: 'lightgray'
                }} exact to={`/main/userlots`}><Icon type="sync" />My lots</NavLink>
                <Menu
                    mode="inline"
                    openKeys={this.state.openKeys}
                    onOpenChange={this.onOpenChange}
                    style={{ width: 256 }}
                >
                    <SubMenu key="sub1" title={<span><Icon type="appstore" /><span>Catalog</span></span>}>
                        <Menu.Item className={styles.disable} key="5">Cars</Menu.Item>
                        <Menu.Item className={styles.disable} key="6">Phones</Menu.Item>
                    </SubMenu>
                    <SubMenu key="sub2" className={styles.disable} title={<span><Icon type="setting" /><span>Settings</span></span>}>
                        <Menu.Item className={styles.disable} key="9">Stats</Menu.Item>
                        <Menu.Item className={styles.disable} key="10">Option 10</Menu.Item>
                        <Menu.Item key="11">Option 11</Menu.Item>
                        <Menu.Item key="12">Option 12</Menu.Item>
                    </SubMenu>
                </Menu>
                <Modal
                    visible={this.state.visibleLot}
                    onOk={this.handleOk}
                    destroyOnClose={true}
                    onCancel={this.handleCancel}
                    footer={[]}
                    width={"600px"}
                >
                    <NewLot onCancel={this.handleCancel}/>
                </Modal>
            </div>
        )
    }
}
const mapStateToProps = (state) => ({
    //is_admin: state.users.user.user.is_admin
});
const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);
export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(Navbar));