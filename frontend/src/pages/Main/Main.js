import React, {Component} from 'react'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {Route, Link, Redirect, Switch, withRouter} from 'react-router-dom'
import PrivateRoute from '../../privateRoute'
import {Layout, Spin} from 'antd';

import { getLots } from "../../actions/lots";
import Navbar from './Navbar/Navbar'
import MainHeader from './Header/Header'
import Dashboard from '../Dashboard/Dashboard'
import Settings from '../Settings/Settings'
import UserLots from '../UserLots/UserLots'
import AuctionLot from '../Dashboard/Lot/AuctionLot'
import styles from './Main.sass'

const {Header, Sider, Content} = Layout;


class Main extends Component {
    constructor(props) {
        super(props)
        this.state= {
            loading: true,
        }
    }

    componentDidMount() {
            this.props.getLots().then(() =>
                this.setState({
                    loading: false
                })
            );
    }
    render() {
        let { loading } = this.state;
        console.log('Loading ',loading);
        return (
            <div className={styles.main}>
                <Layout>

                    <Sider><Navbar/></Sider>
                    <Layout>
                        <Header><MainHeader/></Header>
                        <Content>{ loading ?  <Spin style={{marginTop: '20vh'}} size="large" /> :
                            <Switch>
                                <Redirect exact from='/main' to='/main/dashboard'/>
                                <PrivateRoute path="/main/dashboard" exact auth={this.props.auth} component={Dashboard}/>
                                <PrivateRoute path="/main/settings" exact auth={this.props.auth} component={Settings}/>
                                <PrivateRoute path="/main/userlots" exact auth={this.props.auth} component={UserLots}/>
                                <PrivateRoute path="/main/dashboard/:id" exact auth={this.props.auth} component={AuctionLot}/>
                            </Switch>
                        }</Content>
                    </Layout>
                </Layout>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    auth: state.auth.authenticated
});
const mapDispatchToProps = (dispatch) => bindActionCreators({
    getLots
}, dispatch);
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Main);