import React, {Component} from 'react'
import {Menu, Dropdown, Icon, Badge, Popover, Button} from 'antd';
import {NavLink} from 'react-router-dom'
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import styles from './Header.sass'
import {Link} from 'react-router-dom'
import ethlogo from '../../../assets/images/ethlogo.png'
import { getBalance } from "../../../actions/profile";

const menu = (
    <Menu className={styles.dropdown}>
        <Menu.Item key="0">
            <NavLink className={styles.menu + " " + styles.menuTop} to={`/main/settings`}>
                Account Settings
            </NavLink>
        </Menu.Item>
        <Menu.Item key="1">
            <Link className={styles.menu + " " + styles.menuBottom} to="/signout">Sign out</Link>
        </Menu.Item>
    </Menu>
);

class Header extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        this.props.getBalance()
    }

    render() {
        let searchComponent;

        return (
            <header className={styles.header}>
                <div/>
                <div key={2} className={styles.items}>
                    <div key={1} className={styles.balance}>
                        <img src={ethlogo} alt=""/>{(this.props.balance / 1000000000000000000).toFixed(2)} ETH
                    </div>
                    <div key={2} className={styles.user}>
                        <Dropdown overlay={menu} trigger={['click']} placement={"bottomRight"}>
                            <a className={ " ant-dropdown-link " + styles.username}
                               href="#">{localStorage.getItem('username') ? localStorage.getItem('username') : 'account'}<Icon
                                type="down"/>
                            </a>
                        </Dropdown>
                    </div>
                </div>
            </header>
        )
    }
}

const mapStateToProps = (state) => ({
    balance: state.profile.balance,
});
const mapDispatchToProps = (dispatch) => bindActionCreators({
    getBalance
}, dispatch);
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Header);