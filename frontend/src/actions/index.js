import {UNAUTH_USER, AUTH_USER, ROOT_URL, AUTH_ERROR, FETCH_MESSAGE, GET_BALANCE,} from '../reducers/consts'
import {authError} from './errors'
import fetch from 'isomorphic-fetch'

import { getBalance } from './profile'

function getQueryString(params) {
    let esc = encodeURIComponent;
    return Object.keys(params)
        .map(k => esc(k) + '=' + esc(params[k]))
        .join('&');
}


export function loginUser({username, password}) {
    return function (dispatch) {
        const config =
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    username,
                    password
                })
            }
        return fetch(ROOT_URL + '/api/auth/login/', config).then(function (response) {
            if (response.status >= 400) {
                dispatch(authError('Bad login info'))
                return { error: true }
                /*throw new Error("Bad response from server");*/
            }
            return response.json();
        })
            .then((data) => {
                // -Save the JWT token
                if (data.non_field_errors || data.error) {
                    dispatch(authError('Invalid credentials'))
                    return { error: true }
                } else {
                    localStorage.setItem('token', data.token);
                    localStorage.setItem('username', data.user.username)
                    localStorage.setItem('private_key', data.user.private_key)
                    dispatch({
                        type: AUTH_USER,
                    })
                    return getBalance();
                }
            })
            .catch(() => {
                dispatch(authError('Bad login info'))
                return { error: true }
            })
    }
}

export function registerUser({email, password, username, private_key}) {
    return function (dispatch) {
        const config =
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email,
                    password,
                    username,
                    private_key
                })
            }
        let qs = '?' + getQueryString({ private_key })

        return fetch(ROOT_URL + '/api/smart/balance/' + qs)
            .then(res => {
                if (res.status >= 400) {
                    dispatch(authError('Bad login info'))
                    return { error: true,
                             message: "incorrect number of private key!"
                    }
                    /*throw new Error("Bad response from server");*/
                }
                return res.json();
            })
            .then(resp => {
                if (resp.non_field_errors || resp.error) {
                    return { error: true, message: resp.message ? resp.message : false}
                }
                return fetch(ROOT_URL + '/api/register/', config).then(function (response) {
                    if (response.status >= 400) {
                        return { error: true, message: response.message ? response.message : false }
                        /*throw new Error("Bad response from server");*/
                    }
                    return response.json();
                })
                    .then((data) => {
                        // -Save the JWT token
                        console.log('DATA REG',data);
                        if (data.non_field_errors || data.error) {
                            dispatch(authError(data.message ? data.message : 'Invalid credentials'))
                            return { error: true, message: data.message ? data.message : false }
                        }
                    })
            })
            .catch(() => {
                dispatch(authError('Bad login info'))
                return { error: true }
            })
    }
}

export function signoutUser() {
    localStorage.removeItem('token');
    localStorage.removeItem('username');
    localStorage.removeItem('private_key')
    console.log('SIGN OUTED');
    return {
        type: UNAUTH_USER
    }
}
