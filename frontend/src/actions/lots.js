import {UPDATE_LOTS, ROOT_URL, GET_BETS} from "../reducers/consts";

import fetch from 'isomorphic-fetch';



function getQueryString(params) {
    var esc = encodeURIComponent;
    return Object.keys(params)
        .map(k => esc(k) + '=' + esc(params[k]))
        .join('&');
}

export const getLots = () => (dispatch) => {
    let qs1 = '?' + getQueryString({
        private_key: localStorage.getItem('private_key'),
    })

    return fetch(ROOT_URL + '/api/smart/auctions/' + qs1)
        .then(res => {
            return res.json();
        })
        .then(data => {
            dispatch({
                type: UPDATE_LOTS,
                lots: data.auctions
            });
        })
}

export const createLot = ({ title, description}) => (dispatch) => {
    let qs1 = '?' + getQueryString({
        private_key: localStorage.getItem('private_key'),
        title,
        description
    })


    return fetch(ROOT_URL + '/api/smart/auctions/create/' + qs1)
        .then(res => {
            return res.json();
        })
        .then(resp => {
            let qs2 = '?' + getQueryString({
                private_key: localStorage.getItem('private_key')
            })
            return fetch(ROOT_URL + '/api/smart/auctions/' + qs2)
                .then(res => {
                    return res.json();
                })
                .then(data => {
                    dispatch({
                        type: UPDATE_LOTS,
                        lots: data.auctions
                    });
                })

        })
};

export const getBets = (id) => (dispatch) => {
    let qs1 = '?' + getQueryString({
        private_key: localStorage.getItem('private_key'),
    })


    return fetch(ROOT_URL + `/api/smart/auctions/${id}/bets/` + qs1)
        .then(res => {
            return res.json();
        })
        .then(data => {
            dispatch({
                type: GET_BETS,
                bets: data.bets
            });
        })
}


export const createBet = ({ eth, id }) => (dispatch) => {
    let qs1 = '?' + getQueryString({
        private_key: localStorage.getItem('private_key'),
        wei: eth * 1000000000000000000
    })

    const errors = {
        '-1':'We cant add bet!',
        '1':'Only for first Bet',
        '2':'More money than min price',
        '3': 'Less money than min price + max step',
        '4':'More money ',
        '5':'Less money ',
        '6':'onlyNotSeller ',
        '7':'First pend first bet above min price ',
        '8':'onlyAfterStart',
        '9':'onlyNotAborted ',
        '10':'Low than best bet '
    }
    return fetch(ROOT_URL + `/api/smart/auctions/${id}/bet/` + qs1)
        .then(res => {
            if (res.status == 500)
                return { error: -1 }
            return res.json();
        })
        .then(resp => {
            if (resp.error)
                return errors[resp.error] ? errors[resp.error] : null
            let qs2 = '?' + getQueryString({
                private_key: localStorage.getItem('private_key')
            })
            return fetch(ROOT_URL + `/api/smart/auctions/${id}/bets/` + qs2)
                .then(res => {
                    return res.json();
                })
                .then(data => {
                    dispatch({
                        type: GET_BETS,
                        bets: data.bets
                    });
                })

        })
};

