import {AUTH_ERROR} from "../reducers/consts";
export function authError(error) {
    return {
        type: AUTH_ERROR,
        payload: error
    }
}