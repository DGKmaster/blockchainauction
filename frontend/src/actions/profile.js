import { GET_BALANCE, ROOT_URL } from "../reducers/consts";

import fetch from 'isomorphic-fetch';
import {authError} from "./errors";

function parseJwt (token) {
    let base64Url = token.split('.')[1];
    let base64 = base64Url.replace('-', '+').replace('_', '/');
    return JSON.parse(window.atob(base64));
}

function getQueryString(params) {
    let esc = encodeURIComponent;
    return Object.keys(params)
        .map(k => esc(k) + '=' + esc(params[k]))
        .join('&');
}

export const getBalance = () => (dispatch) => {
    let qs = '?' + getQueryString({ private_key: localStorage.getItem('private_key')})

    return fetch(ROOT_URL + '/api/smart/balance/' + qs)
        .then(res => {
            if (res.status >= 400) {
                dispatch(authError('Bad login info'))
                return { balance: 0 }
                /*throw new Error("Bad response from server");*/
            }
            return res.json();
        })
        .then(resp => {
            dispatch({
                type: GET_BALANCE,
                balance: resp.balance
            });
        })
};

