#!/usr/bin/env bash

python3 manage.py makemigrations
python3 manage.py migrate

echo "[*] Collect static"
python3 /project/manage.py collectstatic -c --noinput > /dev/null

echo "[*] Start uwsgi"
uwsgi --ini /project/deploy/uwsgi.ini
