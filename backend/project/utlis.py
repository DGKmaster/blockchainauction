from django.core.cache import cache
from solc import compile_files
from web3 import Web3

from project.apps.smart.serializer import AuctionSerializer


def get_web3():
    return Web3(Web3.HTTPProvider("http://ganache:8545"))


def get_auction_dispatcher(w3):
    auction_dispatcher = cache.get('auction_dispatcher')
    defaultAccount = w3.eth.defaultAccount

    if auction_dispatcher:
        return w3.eth.contract(**auction_dispatcher)
    else:
        w3.eth.defaultAccount = w3.eth.accounts[0]

    compiled_sols = compile_files([
        '/smart/contracts/Auction.sol',
        '/smart/contracts/AuctionDispatcher.sol'
    ])

    contract_interface = compiled_sols[
        '/smart/contracts/AuctionDispatcher.sol:AuctionDispatcher'
    ]

    w3.eth.defaultAccount = w3.eth.accounts[0]

    AuctionDispatcher = w3.eth.contract(
        abi=contract_interface['abi'],
        bytecode=contract_interface['bin']
    )

    tx_hash = AuctionDispatcher.constructor().transact()
    tx_receipt = w3.eth.waitForTransactionReceipt(tx_hash)

    cache.set('auction_dispatcher', {
        'address': tx_receipt.contractAddress,
        'abi': contract_interface['abi']
    }, 60 * 60 * 24)

    auction_dispatcher = w3.eth.contract(
        address=tx_receipt.contractAddress,
        abi=contract_interface['abi'],
    )

    w3.eth.defaultAccount = defaultAccount

    return auction_dispatcher


def get_auction(w3, contract_address):
    compiled_sols = compile_files(['/smart/contracts/Auction.sol'])

    contract_interface = compiled_sols[
        '/smart/contracts/Auction.sol:Auction'
    ]

    return w3.eth.contract(
        address=contract_address,
        abi=contract_interface['abi'],
    )


def get_auction_info(w3, auction):
    contract = get_auction(w3, auction.tx_hash)

    data = AuctionSerializer(auction).data

    data['returns'] = contract.functions.getReturnInfo().call()

    block_info = contract.functions.getLotInfo().call()

    data['block_info'] = {
        'owner': block_info[0],
        'seller': block_info[1],
        'lot_id': block_info[2],
        'min_price': block_info[3],
        'min_step': block_info[4],
        'max_step': block_info[5],
        'start_time': block_info[6],
        'end_time': block_info[7],
        'aborted': block_info[8],
        'first_bet': block_info[9],
        'finished': block_info[10]
    }

    return data
