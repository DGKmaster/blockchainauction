from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('project.apps.users.urls')),
    path('', include('project.apps.smart.urls')),
]
