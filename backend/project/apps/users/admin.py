from django.contrib import admin

from .models import User


@admin.register(User)
class TranslationAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {'fields': ('username',)}),
        ('Personal info',
         {'fields': ('last_name', 'first_name', 'middle_name', 'email')}),
        ('Permissions', {'fields': (
            'is_active', 'is_staff', 'is_superuser',
        )}),
        ('Important dates', {'fields': ('last_login', 'date_joined')}),
    )
    readonly_fields = ('date_joined', 'last_login',)
    list_display = (
        'id', 'username', 'email', 'last_name', 'first_name', 'middle_name',
        'is_staff',
    )
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'groups',)
    search_fields = (
        'username', 'first_name', 'last_name', 'email',
        'middle_name',
    )
