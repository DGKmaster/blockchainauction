import django_filters
from rest_framework import generics, permissions, filters, status
from rest_framework.response import Response

from project.apps.users.models import User
from project.apps.users.serializer import (
    UserDetailSerializer,
    UserCreateSerializer,
)


class ApiUserListView(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserDetailSerializer
    permission_classes = (permissions.IsAuthenticated,)
    filter_backends = (
        django_filters.rest_framework.DjangoFilterBackend,
        filters.SearchFilter,
    )
    search_fields = ('username', 'first_name', 'last_name', 'middle_name',
                     'email',)
    filter_fields = ('username',)


class ApiUserDetailView(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserDetailSerializer
    permission_classes = (permissions.IsAuthenticated,)


class ApiUserCreateView(generics.CreateAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = UserCreateSerializer

    def post(self, request, *args, **kwargs):
        serializer = UserCreateSerializer(data=request.data)

        if serializer.is_valid():
            user = User.objects.create_user(
                username=serializer.validated_data['username'],
                password=serializer.validated_data['password'],
                email=serializer.validated_data['email'],
                private_key=serializer.validated_data.get('private_key')
            )

            return Response(
                {'user': UserDetailSerializer(user).data},
            )

        return Response(
            {'detail': serializer.errors},
            status=status.HTTP_400_BAD_REQUEST
        )
