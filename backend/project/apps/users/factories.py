import factory
from faker import Factory

from project.apps.users.models import User

fake = Factory.create()


class UserFactory(factory.DjangoModelFactory):
    email = factory.Faker('free_email')
    username = factory.Faker('user_name')
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    middle_name = factory.Faker('first_name')

    class Meta:
        model = User
