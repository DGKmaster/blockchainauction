from django.db import models
from django.contrib.auth.models import AbstractUser
from .managers import UserManager


class User(AbstractUser):
    first_name = models.CharField(max_length=50, blank=True)
    last_name = models.CharField(max_length=50, blank=True)
    middle_name = models.CharField(max_length=50, blank=True)
    private_key = models.CharField(max_length=200, blank=True)

    objects = UserManager()

    def __str__(self):
        return '{} {} {}'.format(
            self.last_name,
            self.first_name,
            self.last_name
        )