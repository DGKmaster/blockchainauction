from rest_framework import serializers

from project.apps.users.models import User


class UserDetailSerializerForAuth(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'first_name', 'last_name',
                  'middle_name', 'private_key',)


class UserListSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'email', 'username', 'private_key',)


class UserDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'first_name', 'last_name',
                  'middle_name', 'private_key',)


class UserCreateSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(required=True)
    username = serializers.CharField(required=True)
    password = serializers.CharField(required=True)
    private_key = serializers.CharField(required=True)

    def save(self, validated_data):
        return User.objects.create(
            email=validated_data.get('email'),
            username=validated_data.get('username'),
            password=validated_data.get('password'),
            private_key=validated_data.get('private_key'),
        )

    class Meta:
        model = User
        fields = ('username', 'password', 'email', 'private_key',)
