import logging

from eth_account import Account
from rest_framework import permissions, status
from rest_framework.response import Response
from rest_framework.views import APIView

from project.apps.smart.models import Auction
from project.apps.smart.serializer import AuctionSerializer
from project.utlis import get_web3, get_auction_dispatcher, get_auction, \
    get_auction_info

logger = logging.getLogger(__name__)


class ApiBetListView(APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, format=None, pk=None):
        auction = Auction.objects.get(pk=pk)
        w3 = get_web3()
        contract = get_auction(w3, auction.tx_hash)

        data = contract.functions.getAllBets().call()

        return Response({
            'bets': data
        })


class ApiCreateBetView(APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, format=None, pk=None):
        w3 = get_web3()

        private_key = request.query_params.get('private_key')
        wei = int(request.query_params.get('wei', w3.toWei(5, 'ether')))

        auction = Auction.objects.get(pk=pk)
        contract = get_auction(w3, auction.tx_hash)

        account = Account.privateKeyToAccount(private_key)

        result = contract.functions.checkBet(wei).call()

        if result != 0:
            return Response({
                'error': result
            }, status=status.HTTP_400_BAD_REQUEST)

        tx_hash = contract.functions.placeBet().transact({
            'from': account.address,
            'value': wei
        })

        tx_receipt = w3.eth.waitForTransactionReceipt(tx_hash)

        data = contract.events.UpdateBetsBuyers().processReceipt(tx_receipt)

        return Response({
            'event': data[0]['event'],
            'buyer': data[0]['args']['e_bestBuyer'],
            'transactionHash': w3.toHex(data[0]['transactionHash']),
            'address': data[0]['address'],
            'blockHash': w3.toHex(data[0]['blockHash']),
        })


class ApiAuctionDetailView(APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, format=None, pk=None):
        auction = Auction.objects.get(pk=pk)
        w3 = get_web3()

        contract = get_auction(w3, auction.tx_hash)

        if contract.functions.checkFinish().call():
            tx_hash = contract.functions.checkFinish().transact()
            w3.eth.waitForTransactionReceipt(tx_hash)

        return Response(get_auction_info(w3, auction))


class ApiAuctionUploadImageView(APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format=None, pk=None):
        auction = Auction.objects.get(pk=pk)
        file = request.FILES.get('file')

        if file.size > 1000 * 1024:
            return Response(
                {'error': 'Big size'},
                status=status.HTTP_400_BAD_REQUEST
            )

        auction.image.delete()
        auction.image = file
        auction.image.save(update_fields=['image'])

        return Response(status=status.HTTP_200_OK)


class ApiCreateAuctionView(APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, format=None):
        w3 = get_web3()

        private_key = request.query_params.get('private_key')
        min_price = int(request.query_params.get('min_price', 3))
        min_step = int(request.query_params.get('min_step', 0))
        max_step = request.query_params.get(
            'max_step',
            w3.toWei(100, 'ether')
        )
        blocks = int(request.query_params.get('blocks', 100))

        account = Account.privateKeyToAccount(private_key)

        auction_dispatcher = get_auction_dispatcher(w3)

        auction = Auction.objects.create(
            title=request.query_params.get('title'),
            description=request.query_params.get('description'),
            address=account.address
        )

        last_block = w3.eth.getBlock('latest')
        role = auction_dispatcher.functions.getUserGroup(account.address).call()

        if not role:
            w3.eth.defaultAccount = w3.eth.accounts[0]

            tx_hash = auction_dispatcher.functions.addUser(
                account.address
            ).transact()

            w3.eth.waitForTransactionReceipt(tx_hash)

            role = 1

        if role < 2:
            w3.eth.defaultAccount = w3.eth.accounts[0]

            tx_hash = auction_dispatcher.functions.changeUserGroup(
                account.address, 2
            ).transact()

            w3.eth.waitForTransactionReceipt(tx_hash)

        w3.eth.defaultAccount = account.address

        auction_dispatcher.functions.createAuction(
            auction.id,
            min_price,
            min_step,
            max_step,
            last_block['number'],
            last_block['number'] + blocks
        ).transact()

        auction.tx_hash = (
            auction_dispatcher.functions.getLastAuction().call()
        )
        auction.save(update_fields=['tx_hash'])

        return Response(AuctionSerializer(auction).data)


class ApiAuctionListView(APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, format=None):
        try:
            w3 = get_web3()
            auction_dispatcher = get_auction_dispatcher(w3)

            auctions = auction_dispatcher.functions.getAuctions().call()

            auctions = Auction.objects.filter(
                tx_hash__in=auctions
            )

            results = []

            for auction in auctions:
                results.append(get_auction_info(w3, auction))

            return Response({
                'auctions': results
            })
        except:
            return Response({
                'auctions': []
            })



class ApiBalanceView(APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, format=None):
        private_key = request.query_params.get('private_key')
        account = Account.privateKeyToAccount(private_key)

        w3 = get_web3()

        return Response({
            'balance': w3.eth.getBalance(account.address)
        })


class ApiSmartView(APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, format=None):
        from solc import compile_source

        # Solidity source code
        contract_source_code = '''
        pragma solidity ^0.4.21;

        contract Greeter {
            string public greeting;

            function Greeter() public {
                greeting = 'Hello';
            }

            function setGreeting(string _greeting) public {
                greeting = _greeting;
            }

            function greet() view public returns (string) {
                return greeting;
            }
        }
        '''

        compiled_sol = compile_source(
            contract_source_code)  # Compiled source code

        # logger.error('************* Truffle *************')
        # logger.error('Truffle: compiling...')
        # logger.error(os.system(
        #     'cd /smart && '
        #     'truffle compile'
        # ))
        #
        #
        # logger.error('Truffle: migrating...')
        # logger.error(os.system(
        #     'cd /smart && '
        #     'truffle migrate —network development'
        # ))
        #
        # name = request.query_params.get('name')
        # file = open('/smart/build/contracts/{}.json'.format(name), 'r')
        # contract = json.loads(file.read())
        # file.close()

        return Response({
            'contract': contract
        })
