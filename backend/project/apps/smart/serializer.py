from rest_framework import serializers

from project.apps.smart.models import Auction


class AuctionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Auction
        fields = ('id', 'title', 'image', 'description', 'tx_hash',
                  'address', 'created_at',)
