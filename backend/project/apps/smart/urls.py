from django.urls import path

from .views import (
    ApiSmartView,
    ApiBalanceView,
    ApiCreateAuctionView,
    ApiAuctionListView,
    ApiCreateBetView,
    ApiAuctionDetailView,
    ApiBetListView,
    ApiAuctionUploadImageView
)

urlpatterns = [
    path('api/smart/compile/', ApiSmartView.as_view()),
    path('api/smart/balance/', ApiBalanceView.as_view()),
    path('api/smart/auctions/create/', ApiCreateAuctionView.as_view()),
    path('api/smart/auctions/<int:pk>/', ApiAuctionDetailView.as_view()),
    path('api/smart/auctions/<int:pk>/bets/', ApiBetListView.as_view()),
    path('api/smart/auctions/<int:pk>/bet/', ApiCreateBetView.as_view()),
    path('api/smart/auctions/', ApiAuctionListView.as_view()),
    path('api/smart/auctions/<int:pk>/upload_image/',
         ApiAuctionUploadImageView.as_view()),
]
